use std::convert::TryInto;
use std::io::Seek;
use chrono::Datelike;
use chrono::TimeZone;
use serde::{Serialize, Deserialize};
use fs2::FileExt;
use std::io::Error;
use std::io::ErrorKind;

fn main() -> Result<(), Box<dyn std::error::Error>>{
    let open_opt: std::fs::OpenOptions = 
        std::fs::OpenOptions::new().read(true).write(true).create(false).to_owned();
    let create_opt = std::fs::OpenOptions::new().read(true).write(true).create(true).to_owned();


    let home_dir = dirs::home_dir().ok_or(Error::new(ErrorKind::Other, "Unable to obtain home directory"))?;
    let storage_dir  = home_dir.as_path().join(std::path::Path::new(".plannr/"));
    if !storage_dir.exists() {
        std::fs::create_dir(storage_dir.as_path())?;
    }

    //Read config from file, or create default if it doesn't exist
    let mut config_file = open_opt.open(storage_dir.as_path().join("config.json"));
    let mut view: ViewState = match &mut config_file {
        Ok(f) => serde_json::from_reader(f).expect("Unable to deserialize config, possibly corrupted file? (you might need to remove config.json)"),
        Err(e) if e.kind() == std::io::ErrorKind::NotFound => {
            println!("config.json does not exist, creating....");
            config_file = create_opt.open(storage_dir.as_path().join("config.json"));
            ViewState{mode: ViewMode::Upcoming, show_done: false}
        }
        Err(e) => return Err(e.to_string().into())
    };

    //Read planner from file, or create default if it doesnt exist
    let mut planner_file = open_opt.open(storage_dir.as_path().join("planner.json"));
    let mut planner: Planner = match &mut planner_file {
        Ok(f) => serde_json::from_reader(f).expect("Unable to deserialize planner, possibly corrupted file? (you might need to remove planner.json)"),
        Err(e) if e.kind() == std::io::ErrorKind::NotFound => {
            println!("planner.json does not exist, creating....");
            planner_file = create_opt.open(storage_dir.as_path().join("planner.json"));
            Planner{recurring: Vec::new(), single: Vec::new()}
        }
        Err(e) => return Err(e.to_string().into())
    };
    
    //refresh weekly items if their due times have passed and they are unfinished
    let now = chrono::Local::now();
    for item in &mut planner.recurring { 
        if item.by < now && item.done { 
            item.done = false;
            item.by = get_next_time(item.period.day, item.period.at);
        }
    }

    //At this point, any errors from opening or creating files should have been handled
    let config_file_u = config_file?;
    let planner_file_u = planner_file?;

    match config_file_u.try_lock_exclusive() {
        Ok(()) => {},
        Err(e) if e.kind() == fs2::lock_contended_error().kind() => return Err("Error: configuration file is locked. Are there any other instances of PlannR running?".into()),
        Err(err) => return Err(err.into())
    }
    
    match planner_file_u.try_lock_exclusive() {
        Ok(()) => {},
        Err(e) if e.kind() == fs2::lock_contended_error().kind() => return Err("Error: planner file is locked. Are there any other instances of PlannR running?".into()),
        Err(err) => return Err(err.into())
    }

    // Filecontext holds files, so that they are always unlocked when the program exits
    // They might unlock anyway, but i would rather not chance it so here it is.
    let _fc = FileContext {file1: &config_file_u, file2: &planner_file_u};

    let stdin = std::io::stdin();
    let mut input_buf = String::with_capacity(200);
    let mut response = String::from("Press enter for a list of commands.");
    let mut run = true;
    while run {
        
        draw_app(&planner, &view, get_term_width(), &response);        

        input_buf.clear(); //clear the input buffer
        response = match stdin.read_line(&mut input_buf){
            Ok(0) => String::from("Enter a command"),
            Ok(_) => process_input(&input_buf.to_lowercase(), &mut planner, &mut view)?,
            Err(err) => format!("Error: {err}"),
        };
        run = if response.as_str() == "||EXIT||" {false} else {true};
        
        //Write to both files
        (&config_file_u).set_len(0)?;
        (&config_file_u).seek(std::io::SeekFrom::Start(0))?;
        (&planner_file_u).set_len(0)?;
        (&planner_file_u).seek(std::io::SeekFrom::Start(0))?;
        match serde_json::ser::to_writer(&config_file_u, &view){ //write serialized data
            Ok(()) => {}
            Err(err) => {println!("Error serializing or writing config: {}", err.to_string())}
        } 
        match serde_json::ser::to_writer(&planner_file_u, &planner){
            Ok(()) => {}
            Err(err) => {println!("Error serializing or writing planner: {}", err.to_string())}
        } 
    }
    Ok(())
}


fn draw_app(planner: &Planner, view: &ViewState, term_width_u: u32, response: &str){
    print!("\x1B[2J\x1B[1;1H"); //clear the terminal
    let term_width = term_width_u as i32;

    let now = chrono::Local::now();
    let date_str = format!("{}", now.format("%A %b %-d"));
    let date_width = date_str.len() as i32;
    let time_str = format!("{}", now.format("%-l:%M%P"));
    let time_width = time_str.len() as i32;

    println!("\u{2554}{}\u{2566}{}\u{2566}{}\u{2557}", get_duplicate_chars(term_width - (time_width + 6 + 2 + date_width), '\u{2550}'), get_duplicate_chars(date_width + 2, '\u{2550}'), get_duplicate_chars(time_width + 2, '\u{2550}'));
    println!("\u{2551} PlannR  {} \u{2551} {} \u{2551} {} \u{2551}", get_duplicate_chars(term_width - (time_width + 18 + date_width), ' '), date_str, time_str);
    println!("\u{2560}{}\u{2569}{}\u{2569}{}\u{2563}", get_duplicate_chars(term_width - (time_width + 6 + 2 + date_width), '\u{2550}'), get_duplicate_chars(date_width + 2, '\u{2550}'), get_duplicate_chars(time_width + 2, '\u{2550}'));
    println!("\u{2551}Recurring Items{}\u{2551}", get_duplicate_chars(term_width - 17, ' '));
    println!("\u{255f}{}\u{2562}", get_duplicate_chars(term_width - 2, '\u{2500}'));
    let recurstr = get_strs(&planner.recurring, view);
    for s in &recurstr {
        println!("\u{2551}{s}\u{2551}");
    } 
    if recurstr.len() <= 3 {
        for _ in 1..(4 - recurstr.len()){
            println!("\u{2551}{}\u{2551}", get_duplicate_chars(term_width - 2, ' '));
        }
    }
    println!("\u{255f}{}\u{2562}", get_duplicate_chars(term_width - 2, '\u{2500}'));
    println!("\u{2551}Single Items{}\u{2551}", get_duplicate_chars(term_width - 14, ' '));
    println!("\u{255f}{}\u{2562}", get_duplicate_chars(term_width - 2, '\u{2500}'));
    let sstr = get_strs(&planner.single, view);
    for s in &sstr {
        println!("\u{2551}{s}\u{2551}");
    }
    if sstr.len() <= 3 {
        for _ in 1..(4 - sstr.len()){
            println!("\u{2551}{}\u{2551}", get_duplicate_chars(term_width - 2, ' '));
        }
    }
    println!("\u{255f}{}\u{2562}", get_duplicate_chars(term_width - 2, '\u{2500}'));
    println!("\u{2551}{response}{}\u{2551}", get_duplicate_chars(term_width - 2 - response.len() as i32, ' '));
    println!("\u{255a}{}\u{255d}", get_duplicate_chars(term_width - 2, '\u{2550}'));
}

fn get_strs(v: &Vec<impl PlanItem>, view: &ViewState) -> Vec<String> {
    let mut ordered = get_ordered(v, get_term_width() - 52);
    let sorted = match view.mode{ // Sort items depending on the view mode
        ViewMode::Index => ordered,
        ViewMode::Upcoming => {
            ordered.sort_by(|(item1, _),(item2, _)| item1.by().timestamp().cmp(&item2.by().timestamp()));
            ordered
        }
    };
    let filtered = match view.show_done { //Filter out done items if show_done is false
        true => sorted,
        false => sorted.into_iter().filter(|(item, _)| !item.done()).collect()
    };
    return filtered.into_iter().map(|(_, istr)| istr).collect();
}

fn get_ordered<T: PlanItem>(v: &Vec<T>, desc_max: u32) -> Vec<(&T, String)> {
    v.into_iter().enumerate().map(|(it, p_item)| (p_item, p_item.item_str(desc_max, it))).collect()
}

fn get_term_width() -> u32 {
    match terminal_size::terminal_size(){
        Some((terminal_size::Width(w), _)) => w.into(),
        _ => 65,
    }
}

/**
 * Processes input from user.
 * Returns a result containing either a response for the user,
 * or an error which will halt the program.
 * Returns an ok result for invalid input, but returns an error for any unrecoverable system error
 * Many errors inside here will be returned as Ok, because they are meant to be presented to the
 * user without terminating the program.
 */
fn process_input(input: &str, planner: &mut Planner, view: &mut ViewState) -> Result<String, Box<dyn std::error::Error>> {
    let tokens: Vec<String> = input.chars().filter(|x| x != &'\n' && x != &'\r').collect::<String>().split(' ').map(|x| x.to_owned()).collect();
   let stdin = std::io::stdin();
   /* Command Tree
    *
    * Side Note: all these calls to to_owned and get_token are basically a giant rust moment.
    * The match tree compares against &strs because of their non-allocating, static nature.
    * They are essentially just built in to the code, which is good because the comparisons are
    * called a lot.
    * The method needs to return a String because it needs an owned value to live long enough to
    * return.
    * So all of those to_owneds arent a problem because for any set of tokens given to the match
    * tree, only one resultant string is returned, and thus, only one allocation happens.
    *
    */
   match get_token(&tokens, 0){
    Some("r") | Some ("recurring") => 
        match get_token(&tokens, 1){
            Some("a") | Some("add") =>
                {
                    println!("Enter a description for your item");
                    let mut input = String::new();
                    match stdin.read_line(&mut input) {
                        Err(err) => Err(Box::new(err)),
                        Ok(0) => Ok("Enter a description.".to_owned()), //to_strings -> rust moment
                        Ok(_) => {
                            println!("On what day does this item repeat? (Sun-Sat: 1-7)");
                            let mut in2 = String::new();
                            match stdin.read_line(&mut in2){
                                Err(err) => Err(Box::new(err)),
                                Ok(0) => Ok("Enter a valid number from 1 to 7.".into()),
                                Ok(_) =>
                                {
                                    let day: Result<chrono::Weekday, Box<dyn std::error::Error>> = match in2.trim().parse::<u8>()
                                    
                                    {
                                        Err(err) => Err(Box::new(err)),
                                        Ok(1) => Ok(chrono::Weekday::Sun),
                                        Ok(2) => Ok(chrono::Weekday::Mon),
                                        Ok(3) => Ok(chrono::Weekday::Tue),
                                        Ok(4) => Ok(chrono::Weekday::Wed),
                                        Ok(5) => Ok(chrono::Weekday::Thu),
                                        Ok(6) => Ok(chrono::Weekday::Fri),
                                        Ok(7) => Ok(chrono::Weekday::Sat),
                                        _ => Err("Enter a valid number from 1 to 7.".into())
                                    };

                                    match day {
                                        Err(err) => Ok(err.to_string()),
                                        Ok(d) => {
                                            println!("At what time is this item");
                                            let mut in3 = String::new();
                                            match stdin.read_line(&mut in3){
                                                Err(err) => Err(Box::new(err)),
                                                Ok(0) => Ok("Enter a valid number from 0 to 23".into()),
                                                Ok(_) => {
                                                    let hr = in3.trim().parse::<u8>();
                                                    match hr {
                                                        Err(err) => Ok(err.to_string()),
                                                        Ok(h) =>
                                                            if h < 24 {       
                                                                planner.recurring.push(RecurringPlanItem{
                                                                    desc: input.trim().to_string(),
                                                                    period: RecurrencePeriod{day: d, at: h},
                                                                    by: get_next_time(d, h), done: false,
                                                                });
                                                                Ok("Successfully added item.".to_owned())
                                                            } else {
                                                                Ok("Enter a valid number from 0 to 24".to_owned())
                                                            }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            Some("r") | Some("remove") => 
            {
                match parse_token_int(&tokens, 2) 
                {
                   Ok(index) =>
                   {
                        if index > 0 && index <= planner.recurring.len().try_into()? {
                            planner.recurring.remove(index as usize - 1);
                            Ok("Successfully removed item.".to_owned())
                        } else {
                            Ok(format!("Enter an index between 1 and {}", planner.recurring.len()))
                        }
                   },
                   Err(err) => Ok(err.to_string()),
                }
            }
            Some("f") | Some("finish") =>
            {
                match parse_token_int(&tokens, 2)
                {
                    Ok(index) =>
                        if index > 0 && index <= planner.recurring.len().try_into().unwrap() {
                            let item = planner.recurring.get_mut(index as usize - 1).unwrap();
                            if item.by <= chrono::Local::now() { //don't set to done if new item is ready
                                item.by = get_next_time(item.period.day, item.period.at); 
                            } else {
                                item.done = true;
                            }
                            Ok("Successfully set item to done.".to_owned())
                        } else {
                            Ok(format!("Enter an index between 1 and {}", planner.recurring.len()))
                        }
                    Err(err) => Ok(err.to_string())
                }

            }
            _ => Ok("Enter a valid command ((a)dd, (r)emove, (f)inish)".to_owned())
        }
    Some("s") | Some("single") =>
        match get_token(&tokens, 1){
            Some("a") | Some("add") => {
                println!("Enter a description for your item:");
                let mut input = String::new();
                match stdin.read_line(&mut input) {
                    Err(err) => Err(Box::new(err)),
                    Ok(0) => Ok("Enter a description.".to_owned()),
                    Ok(_) => {
                        let mut in2 = String::new();
                        println!("Enter what date this item is due");
                        stdin.read_line(&mut in2)?;    
                        match get_next_date(in2.as_str())? {
                            Err(e) => Ok(e.to_string()),
                            Ok(d) => {
                                println!("Enter the hour at which the item is due.");
                                let mut hr_in = String::new();
                                match stdin.read_line(&mut hr_in) {
                                    Err(err) => Err(Box::new(err)),
                                    Ok(0) => Ok("Enter a number between 0 and 23.".to_owned()),
                                    Ok(_) => {
                                        match hr_in.trim().parse::<u8>() {
                                            Err(err) => Ok(err.to_string()),
                                            Ok(hr) => {
                                                if hr < 24{
                                                    let dt = d.and_hms(hr.into(), 0, 0);
                                                    planner.single.push(SinglePlanItem { desc: input.trim().to_string(), by: dt});
                                                    Ok("Successfully added item.".to_owned())
                                                } else {
                                                    Ok("Enter a number between 0 and 23".to_owned())
                                                }
                                            } 
                                        }
                                    }
                                }
                            } 
                        }
                    }
                }
            }
            Some("r") | Some("remove") => 
            {
                match parse_token_int(&tokens, 2) 
                {
                   Ok(index) =>
                   {
                        if index > 0 && index <= planner.recurring.len().try_into().unwrap() {
                            planner.single.remove(index as usize - 1);
                            Ok("Successfully removed item.".to_owned())
                        } else {
                            Ok(format!("Enter an index between 1 and {}", planner.recurring.len()))
                        }
                   },
                   Err(err) => Ok(err.to_string()),
                }
            }
            _ => Ok("Enter a valid command((a)dd, (r)emove)".to_owned())
        }
    Some("v") | Some("view") =>
        match get_token(&tokens, 1){
            Some("m") | Some("mode") => {
                match get_token(&tokens, 2) {
                    Some("u") | Some("upcoming") => {view.mode = ViewMode::Upcoming; Ok("Set view mode to upcoming".to_owned()) },
                    Some("i") | Some("index") => {view.mode = ViewMode::Index; Ok("Set view mode to index".to_owned()) },
                    _ => Ok("Enter a valid option ((u)pcoming, (i)ndex)".to_owned())
                }   
            }
            Some("d") | Some ("done") => { view.show_done = !view.show_done; Ok("Toggled showing finished tasks".to_owned()) }
            _ => Ok("Enter a valid command ((m)ode, (d)one)".to_owned())
        }
    Some("q") | Some("quit") => Ok("||EXIT||".to_owned()),
    _ => Ok("Enter a valid command ((r)ecurring, (s)ingle, (v)iew, (q)uit)".to_owned()),
   }
}

fn parse_token_int (tokens: &Vec<String>, index: u32) -> Result<u32, String> {
    tokens.get(index as usize).ok_or("Error while getting input")?.trim().parse::<u32>().map_err(|e| e.to_string())
}

fn get_token (tokens: &Vec<String>, index: u32) -> Option<&str> {
    match tokens.get(index as usize) {
        Some(s) => Some(s.as_str()),
        None => None
    }
}
/**
 * Returns a doubly wrapped error whose outer error is unrecoverable and will halt the program, but
 * whose inner error will be displayed to the user.
 */
fn get_next_date(inp: &str) -> Result<Result<chrono::Date<chrono::Local>, Box<dyn std::error::Error>>, Box<dyn std::error::Error>> {
    let re_date_full = regex::Regex::new(r"^\d{4}[-/ ]\d{1,2}[-/ ]\d{1,2}$")?;
    let re_date_month = regex::Regex::new(r"^\d{1,2}[-/ ]\d{1,2}$")?;
    let re_date_days = regex::Regex::new(r"^\d{1,2}$")?;
    let impossible_err = Error::new(ErrorKind::Other, "This shouldn't be possible");
    if re_date_full.is_match(inp.trim()) {
        let mut spparse: std::collections::VecDeque<Result<u32, std::num::ParseIntError>> = inp.split(&[' ', '/', '-']).map(|s| s.trim().parse::<u32>()).collect();
        let y = match spparse.pop_front().ok_or(impossible_err.to_string())? {Ok(a) => a, Err(err) => {return Ok(Err(Box::new(err)))}}; //The rust moment ever
        let m = match spparse.pop_front().ok_or(impossible_err.to_string())? {Ok(a) => a, Err(err) => {return Ok(Err(Box::new(err)))}};
        let d = match spparse.pop_front().ok_or(impossible_err.to_string())? {Ok(a) => a, Err(err) => {return Ok(Err(Box::new(err)))}};
        return Ok(Ok(chrono::Local.from_local_date(&chrono::NaiveDate::from_ymd_opt(y.try_into()?, m, d)
            .ok_or(std::io::Error::new(std::io::ErrorKind::Other, format!("Error creating date {y}/{m}/{d}")))?)
            .earliest().ok_or(Box::new(Error::new(ErrorKind::Other, "Error getting time zone")))?)); // rust moment
    } else if re_date_month.is_match(&inp.trim()) {
        let mut spparse: std::collections::VecDeque<Result<u32, std::num::ParseIntError>> = inp.split(&[' ', '/', '-']).map(|s| s.trim().parse::<u32>()).collect();
        let today = chrono::Local::now().naive_local().date();
        let m = match spparse.pop_front().ok_or(impossible_err.to_string())? {Ok(a) => a, Err(err) => {return Ok(Err(Box::new(err)))}};
        let d = match spparse.pop_front().ok_or(impossible_err.to_string())? {Ok(a) => a, Err(err) => {return Ok(Err(Box::new(err)))}};
        // increment year if month and day have already passed
        let y = if today >= chrono::NaiveDate::from_ymd(today.year(), m, d) {today.year() + 1} else {today.year()};
        return Ok(Ok(chrono::Local.from_local_date(&chrono::NaiveDate::from_ymd_opt(y, m, d)
            .ok_or(std::io::Error::new(std::io::ErrorKind::Other, format!("Error creating date {y}/{m}/{d}")))?)
            .earliest().ok_or(Box::new(Error::new(ErrorKind::Other, "Error getting time zone")))?)); // rust moment
    } else if re_date_days.is_match(&inp.trim()) {
        let today = chrono::Local::now().naive_local().date();
        let d = match inp.trim().parse::<u32>() { Ok(a) => a, Err(err) => {return Ok(Err(Box::new(err)))}}; 
        let mut m = today.month();
        let mut y = today.year();
        if today.day() >= d {
            if m == 12 { //Increment year
                y += 1;
                m = 1;
            } else {
                m += 1;
            }
        }
        return Ok(Ok(chrono::Local.from_local_date(&chrono::NaiveDate::from_ymd_opt(y, m, d)
            .ok_or(std::io::Error::new(std::io::ErrorKind::Other, format!("Error creating date {y}/{m}/{d}")))?)
            .earliest().ok_or(Box::new(Error::new(ErrorKind::Other, "Error getting time zone")))?)); // rust moment
    } else {
        Ok(Err("Invalid date format (yyyy/mm/dd, mm/dd, dd)".into()))
    }
}


fn get_next_time(day: chrono::Weekday, hour: u8) -> DateTime{
    let current_time: chrono::DateTime<chrono::Local> = chrono::offset::Local::now();
    let current_year = current_time.year();
    let current_week = current_time.iso_week().week();
    let current_day = current_time.weekday();
    // if day has already passed this week, set next occurrence to next week
    let mut adj_week = if day.number_from_monday() as i32 - current_day.number_from_monday() as i32 <= 0 {println!("next"); current_week + 1} else {println!("this"); current_week};
    let adj_year = if adj_week > 53 { adj_week = 1; current_year + 1 } else {current_year};
    return chrono::Local.from_local_datetime(&chrono::NaiveDate::from_isoywd(adj_year, adj_week, day).and_hms(hour as u32, 0, 0)).unwrap();
}

type DateTime = chrono::DateTime<chrono::Local>;

/**
 * Holds the state of the planner
 */
#[derive(Serialize, Deserialize)]
struct Planner{
    recurring: Vec<RecurringPlanItem>,
    single: Vec<SinglePlanItem>
}
/**
 * An item in the planner
 */
#[derive(Serialize, Deserialize)]
struct SinglePlanItem {
    desc: String,
    by: DateTime,
}

#[derive(Serialize, Deserialize)]
struct RecurringPlanItem {
    desc: String,
    period: RecurrencePeriod,
    done: bool,
    by: DateTime,
}

#[derive(Serialize, Deserialize)]
struct RecurrencePeriod {
    day: chrono::Weekday,
    at: u8
}

#[derive(Serialize, Deserialize)]
struct ViewState {
    mode: ViewMode,
    show_done: bool,
}

#[derive(Serialize, Deserialize)]
enum ViewMode{
    Index,
    Upcoming,
}


trait PlanItem{
    fn desc_str(&self, max: u32) -> String;
    fn item_str(&self, max: u32, index: usize) -> String;
    fn by(&self) -> chrono::DateTime<chrono::Local>;
    fn done(&self) -> bool;
}

impl PlanItem for SinglePlanItem{
    fn desc_str(&self, max: u32) -> String{
        self.desc.chars().into_iter().take(max as usize).collect()
    }
    fn by(&self) -> DateTime {self.by} 
    fn item_str(&self, max: u32, index: usize) -> String{
        format!("{}:     {}{}", right_pad(index + 1, 2), 
                right_pad(self.desc_str(max), max as i32), get_time_str(&self.by()))
    }
    fn done(&self) -> bool {false}
}

impl PlanItem for RecurringPlanItem{
    fn desc_str(&self, max: u32) -> String{
        self.desc.chars().into_iter().take(max as usize).collect()
    }
    fn by(&self) -> DateTime {self.by} 
    fn item_str(&self, max: u32, index: usize) -> String{
        format!("{}: [{}] {}{}", left_pad(index + 1, 2), if self.done {"\u{2713}"} else {" "},
                right_pad(self.desc_str(max), max as i32), get_time_str(&self.by()) )
    }
    fn done(&self) -> bool {self.done}
}

fn get_time_str(time: &DateTime) -> String{
    let now = chrono::Local::now();
    let diff = *time - now;
    let mut rel = if diff.num_hours() >= 48 {
        right_pad(format!("In {} days on", right_pad(diff.num_days().to_string(), 2)), 15)
    } else if diff.num_hours() >= 0 {
        right_pad(format!("In {} hours on" , right_pad(diff.num_hours().to_string(), 2)), 15)
    } else if diff.num_hours() >= -24 {
        right_pad(format!("{} hours ago", right_pad((-diff.num_hours()).to_string(), 2)), 15)
    } else {
        right_pad(format!("{} days ago", right_pad((-diff.num_days()).to_string(), 2)), 15)
    };
    rel.push_str(&right_pad(format!("{}", time.format("%A %b %d at %-l%P")), 27));
    return rel;
}

fn right_pad(s: impl ToString, len: i32) -> String{
    let mut pushed = s.to_string();
    if pushed.len() >= len as usize { return pushed; }
    pushed.push_str(get_duplicate_chars(len as i32 - pushed.len() as i32, ' ').as_str());
    return pushed;
}

fn left_pad(s: impl ToString, len: i32) -> String {
    let push = s.to_string();
    if push.len() >= len as usize { return push.to_string(); }
    let mut padding = get_duplicate_chars(len - push.len() as i32, ' ');
    padding.push_str(&push);
    return padding;
}

fn get_duplicate_chars(num: i32, character: char) -> String{
    let mut b = String::new();
    if num <= 0 { return b };
    for _ in 0..num{
        b.push(character);
    }
    return b;
}

struct FileContext<'a> {
    file1: &'a std::fs::File,
    file2: &'a std::fs::File,
}

impl Drop for FileContext<'_> {
    fn drop(&mut self){
        self.file1.unlock().unwrap();
        self.file2.unlock().unwrap();
    }
}
