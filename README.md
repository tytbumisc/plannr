# PlannR
A small personal planner, for marking down weekly recurring and singular items.

![PlannR Example](https://gitlab.com/tytbumisc/plannr/-/raw/main/plannr_ex.png)

## Build Instructions:
Requires [Rust](https://www.rust-lang.org/tools/install)

`git clone https://gitlab.com/tytbumisc/plannr`
`cd plannr`
`cargo build -r`

PlannR saves its data in a a directory called '.plannr' within your home directory.

## Command Tree:

Commands are inputted in one line, for example, to add a recurring item, enter `r a`, or to remove the second recurring item, enter `r r 2`

(r)ecurring: Operations on recurring items, which refresh weekly
 -> (a)dd: add an item
 -> (r)emove: remove an item
 -> (f)inish: mark an item as done for the week

(s)ingle: Operations on non-recurring items.
 -> (a)dd: add an item
 -> (r)emove: remove an item

(v)iew: Change view settings.
 -> (m)ode: Change how the items are ordered.
  -> (i)ndex: Sort items by their index
  -> (u)pcoming: Sort items by their date
 -> (d)one: Toggle whether or not to show finished recurring items

(q)uit: exit PlannR
